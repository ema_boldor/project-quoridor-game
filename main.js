// -1 = gard; 0 = no gard; 1 = pozitie; 3 = pion1; 4 = pion2
var a = [
    [1, 0, 1, 0, 1, 0, 1, 0, 3, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 4, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3],
];

var xUser = 507;
var yUser = 500;

var xComp = 507;
var yComp = 100;
var ziduri1 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
var ziduri2 = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1];
var input1;
var input2;
var moveWhite = false;
var Player1;
var moveBlack = false;
var Player2;
var roundPlayer = 1;
var xWhite;
var yWhite;
var xBlack;
var yBlack;
var jump = 2;
var vGard = false;
var hGard = false;
var cui;
var cuj;
var result = [];
var st;
var i,j;
var x,y;
var zid1=9;
var zid2=9;
var comp = 0;

let button;

function setup() {
    createCanvas(800, 600);

    button = createButton('Resart');
    button.position(1000, 500);
    button.mousePressed(resetGame);
    button.size(200);

    button1 = createButton('Add First Player');
    button1.position(900, 300);
    button1.size(135, 60);
    button1.style('background-color', this.buttonColor);
    button1.mousePressed(displayPlayer1);

    button2 = createButton('Add Second Player');
    button2.position(900, 350);
    button2.size(135, 60);
    button2.style('background-color', this.buttonColor);
    button2.mousePressed(displayPlayer2);

    button3 = createButton('moveWhite');
    button3.position(50, 30);
    button3.size(100);
    button3.mousePressed(White);

    button4 = createButton('moveBlack');
    button4.position(50, 470);
    button4.size(100);
    button4.mousePressed(Black);

    button5 = createButton('verticalGard');
    button5.position(50, 60);
    button5.size(100);
    button5.mousePressed(vGard1);

    button6 = createButton('horizontalGard');
    button6.position(50, 90);
    button6.size(100);
    button6.mousePressed(hGard1);

    button7 = createButton('verticalGard');
    button7.position(50, 500);
    button7.size(100);
    button7.mousePressed(vGard2);

    button8 = createButton('horizontalGard');
    button8.position(50, 530);
    button8.size(100);
    button8.mousePressed(hGard2);

    button9 = createButton('vs computer');
    button9.position(815,120);
    button9.size(100);
    button9.mousePressed(computergame);

}

function computergame(){
    
}

function displayPlayer1() {
    input1 = createInput("Player 1");
    input1.position(1140, 300);
    Player1 = input1.value();

}

function displayPlayer2() {
    input2 = createInput("Player 2");
    input2.position(1140, 350);
    Player2 = input2.value();

}


function resetGame() {
    xUser = 507;
    yUser = 500;

    xComp = 507;
    yComp = 100;

    a = [
    [1, 0, 1, 0, 1, 0, 1, 0, 3, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -3],
    [1, 0, 1, 0, 1, 0, 1, 0, 4, 0, 1, 0, 1, 0, 1, 0, 1, -3],
    [-3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3, -3],
    ];
}


function draw() {
    // background("gray");
    insert();
}

function insert() {
    Table.matrice();
    Table.pozitieTabla();
    Table.ziduri();
    pion1(xComp, yComp);
    pion2(xUser, yUser);
    if (input1) {
        Player1 = input1.value();
        if (yComp == 500) {
            textSize(50);
            textStyle(BOLD);
            text(Player1 + " ai castigat !", 300, 300);
        }
    }
    if (input2) {
        Player2 = input2.value();
        if (yUser == 100) {
            textSize(50);
            textStyle(BOLD);
            text(Player2 + " ai castigat !", 300, 300);
        }
    }
    if (vGard) {
        fill('grey');
        rect(mouseX - 45, mouseY, 90, 10);
    }
    if (hGard) {
        fill('grey');
        rect(mouseX, mouseY - 45, 10, 90);
    }
    fill('grey');
    for (let j = 0; j < 17; j++)
        for (let i = 0; i < 17; i++) {
            if (a[i][j] == -1)
                if (i % 2 == 1 && j % 2 == 1)
                    rect(263 + j * 25, 95 + i * 25, 90, 10);
            if (a[i][j] == -2)
                if (i % 2 == 1 && j % 2 == 1)
                    rect(303 + j * 25, 55 + i * 25, 10, 90);
        }
    text(roundPlayer,10,200);
}

function mouseClicked() {
    for (let i = 0; i < 17; i++)
        for (let j = 0; j < 17; j++) {
            if (a[i][j] == 3) {
                xWhite = i;
                yWhite = j
            }
            if (a[i][j] == 4) {
                xBlack = i;
                yBlack = j
            }
        }
    jump = 2;
    //pion alb
    if (roundPlayer == 1)
        if (moveWhite) {
            if (mouseX < 727 && mouseX > 287 && mouseY > 80 && mouseY < 520) {
                //dreapta
                if (roundPlayer == 1 && mouseX >= xComp + 30 && mouseX <= xComp + 70 && mouseY >= yComp - 20 && mouseY <= yComp + 20 && a[xWhite][yWhite + 1] > -1)
                    if (a[xWhite][yWhite + 2] == 4 && a[xWhite][yWhite + 3] > -1 && a[xWhite][yWhite + 4] == 1) {
                        jump = 4;
                        xComp = xComp + 100;
                        roundPlayer = 2;
                        a[xWhite][yWhite] = 1;
                        a[xWhite][yWhite + jump] = 3;
                    }
                else {
                    jump = 2;
                    xComp = xComp + 50;
                    roundPlayer = 2;
                    a[xWhite][yWhite] = 1;
                    a[xWhite][yWhite + jump] = 3
                }
                //stanga
                if (roundPlayer == 1 && mouseX >= xComp - 70 && mouseX <= xComp - 30 && mouseY >= yComp - 20 && mouseY <= yComp + 20 && a[xWhite][yWhite - 1] > -1)
                    if (a[xWhite][yWhite - 2] == 4 && a[xWhite][yWhite - 3] > -1 && a[xWhite][yWhite - 4] == 1) {
                        jump = 4;
                        xComp = xComp - 100;
                        roundPlayer = 2;
                        a[xWhite][yWhite] = 1;
                        a[xWhite][max(0, yWhite - jump)] = 3
                    }
                else {
                    jump = 2;
                    xComp = xComp - 50;
                    roundPlayer = 2;
                    a[xWhite][yWhite] = 1;
                    a[xWhite][max(0, yWhite - jump)] = 3
                }
                //spate
                if (roundPlayer == 1 && mouseX >= xComp - 20 && mouseX <= xComp + 20 && mouseY >= yComp - 70 && mouseY <= yComp - 30 && a[xWhite - 1][yWhite] > -1)
                    if (a[xWhite - 2][yWhite] == 4 && a[xWhite - 3][yWhite] > -1 && a[xWhite - 4][yWhite] == 1) {
                        jump = 4;
                        yComp = yComp - 100;
                        roundPlayer = 2;
                        a[xWhite][yWhite] = 1;
                        a[max(0, xWhite - jump)][yWhite] = 3
                    }
                else {
                    jump = 2;
                    yComp = yComp - 50;
                    roundPlayer = 2;
                    a[xWhite][yWhite] = 1;
                    a[max(0, xWhite - jump)][yWhite] = 3
                }
                //fata
                if (roundPlayer == 1 && mouseX >= xComp - 20 && mouseX <= xComp + 20 && mouseY >= yComp + 30 && mouseY <= yComp + 70 && a[xWhite + 1][yWhite] > -1)
                    if (a[xWhite + 2][yWhite] == 4 && a[xWhite + 3][yWhite] > -1 && a[xWhite + 4][yWhite] == 1) {
                        jump = 4;
                        yComp = yComp + 100;
                        roundPlayer = 2;
                        a[xWhite][yWhite] = 1;
                        a[xWhite + jump][yWhite] = 3
                    }
                else {
                    jump = 2;
                    yComp = yComp + 50;
                    roundPlayer = 2;
                    a[xWhite][yWhite] = 1;
                    a[xWhite + jump][yWhite] = 3
                }
            }
        }


    //pion negru
    if (roundPlayer == 2)
        if (moveBlack) {
            if (mouseX < 727 && mouseX > 287 && mouseY > 80 && mouseY < 520) {
                //dreapta
                if (roundPlayer == 2 && mouseX >= xUser + 30 && mouseX <= xUser + 70 && mouseY >= yUser - 20 && mouseY <= yUser + 20 && a[xBlack][yBlack + 1] > -1)
                    if (a[xBlack][yBlack + 2] == 3 && a[xBlack][yBlack + 3] > -1 && a[xBlack][yBlack + 4] == 1) {
                        jump = 4;
                        xUser = xUser + 100;
                        roundPlayer = 1;
                        a[xBlack][yBlack] = 1;
                        a[xBlack][yBlack + jump] = 4
                    }
                else {
                    jump = 2;
                    xUser = xUser + 50;
                    roundPlayer = 1;
                    a[xBlack][yBlack] = 1;
                    a[xBlack][yBlack + jump] = 4
                }
                //stanga
                if (roundPlayer == 2 && mouseX >= xUser - 70 && mouseX <= xUser - 30 && mouseY >= yUser - 20 && mouseY <= yUser + 20 && a[xBlack][yBlack - 1] > -1)
                    if (a[xBlack][yBlack - 2] == 3 && a[xBlack][yBlack - 3] > -1 && a[xBlack][yBlack - 4] == 1) {
                        jump = 4;
                        xUser = xUser - 100;
                        roundPlayer = 1;
                        a[xBlack][yBlack] = 1;
                        a[xBlack][max(0, yBlack - jump)] = 4
                    }
                else {
                    jump = 2;
                    xUser = xUser - 50;
                    roundPlayer = 1;
                    a[xBlack][yBlack] = 1;
                    a[xBlack][max(0, yBlack - jump)] = 4
                }
                //fata
                if (roundPlayer == 2 && mouseX >= xUser - 20 && mouseX <= xUser + 20 && mouseY >= yUser - 70 && mouseY <= yUser - 30 && a[xBlack - 1][yBlack] > -1)
                    if (a[xBlack - 2][yBlack] == 3 && a[xBlack - 3][yBlack] > -1 && a[xBlack - 4][yBlack] == 1) {
                        jump = 4;
                        yUser = yUser - 100;
                        roundPlayer = 1;
                        a[xBlack][yBlack] = 1;
                        a[max(0, xBlack - jump)][yBlack] = 4
                    }
                else {
                    jump = 2;
                    yUser = yUser - 50;
                    roundPlayer = 1;
                    a[xBlack][yBlack] = 1;
                    a[max(0, xBlack - jump)][yBlack] = 4
                }
                //spate
                if (roundPlayer == 2 && mouseX >= xUser - 20 && mouseX <= xUser + 20 && mouseY >= yUser + 30 && mouseY <= yUser + 70 && a[xBlack + 1][yBlack] > -1) {
                    if (a[xBlack + 2][yBlack] == 3 && a[xBlack + 3][yBlack] > -1 && a[xBlack + 4][yBlack] == 1) {
                        jump = 4;
                        yUser = yUser + 100;
                        roundPlayer = 1;
                        a[xBlack][yBlack] = 1;
                        a[xBlack + jump][yBlack] = 4
                    } else {
                        jump = 2;
                        yUser = yUser + 50;
                        roundPlayer = 1;
                        a[xBlack][yBlack] = 1;
                        a[xBlack + jump][yBlack] = 4
                    }
                }
            }
        }

    if (roundPlayer == 1) {
        if(ziduri1[0]==1)
        for (i = 0; i < 17; i++)
            for (j = 0; j < 17; j++)
                if (i % 2 == 1 && j % 2 == 1) {
                    if (mouseX >= 300 + j * 25 && mouseX <= 310 + j * 25 && mouseY >= 90 + i * 25 && mouseY <= 105 + i * 25) {
                        x=i;y=j;
                        if (vGard) {
                            if(a[x][y] == 0 && a[x][y - 1] == 0 && a[x][y + 1] == 0)
                           { a[x][y] = -1;
                            a[x][y - 1] = -1;
                            a[x][y + 1] = -1;
                            roundPlayer = 2;
                            vGard = false;
                            ziduri1[zid1]=0;
                            zid1--;
                        }

                            verify(3);
                            if (st) {
                                a[x][y] = 0;
                                a[x][y - 1] = 0;
                                a[x][y + 1] = 0;
                                roundPlayer = 1;
                                vGard = true;
                                zid1++;
                                ziduri1[zid1]=1;
                        }
                            }
                            verify(4);
                            if (st) {
                                a[x][y] = 0;
                                a[x][y - 1] = 0;
                                a[x][y + 1] = 0;
                                roundPlayer = 1;
                                vGard = true;
                                zid1++;
                                ziduri1[zid1]=1;
                            }
                        if (hGard) {
                            if(a[x][y] == 0 && a[x-1][y]== 0 && a[x+1][y] == 0)
                            {a[x][y] = -2;
                            a[x - 1][y] = -2;
                            a[x + 1][y] = -2;
                            roundPlayer = 2;
                            hGard = false;
                            ziduri1[zid1]=0;
                            zid1--;
                        }

                            verify(3);
                            if (st) {
                                a[x][y] = 0;
                                a[x - 1][y] = 0;
                                a[x + 1][y] = 0;
                                roundPlayer = 1;
                                hGard = true;
                                zid1++;
                                ziduri1[zid1]=1;
                            }
                            verify(4);
                            if (st) {
                                a[x][y] = 0;
                                a[x - 1][y] = 0;
                                a[x + 1][y] = 0;
                                roundPlayer = 1;
                                hGard = true;
                                zid1++;
                                ziduri1[zid1]=1;
                            }
                        }
                    }
                }
            }
    if (roundPlayer == 2) {
        if(ziduri2[0]==1)
        for (i = 0; i < 17; i++)
            for (j = 0; j < 17; j++)
                if (i % 2 == 1 && j % 2 == 1) {
                    if (mouseX >= 300 + j * 25 && mouseX <= 310 + j * 25 && mouseY >= 90 + i * 25 && mouseY <= 105 + i * 25) {
                        x=i;y=j;
                        if (vGard) {
                            if(a[x][y] == 0 && a[x][y - 1] == 0 && a[x][y + 1] == 0)
                           { a[x][y] = -1;
                            a[x][y - 1] = -1;
                            a[x][y + 1] = -1;
                            roundPlayer = 1;
                            vGard = false;
                            ziduri2[zid2]=0;
                            zid2--;
                        }

                            verify(3);
                            if (st) {
                                a[x][y] = 0;
                                a[x][y - 1] = 0;
                                a[x][y + 1] = 0;
                                roundPlayer = 2;
                                vGard = true;
                                zid2++;
                                ziduri2[zid2]=1;
                            }
                            verify(4);
                            if (st) {
                                a[x][y] = 0;
                                a[x][y - 1] = 0;
                                a[x][y + 1] = 0;
                                roundPlayer = 2;
                                vGard = true;
                                zid2++;
                                ziduri2[zid2]=1;
                            }
                        }
                        if (hGard) {
                            if(a[x][y] == 0 && a[x-1][y] == 0 && a[x+1][y] == 0)
                            {a[x][y] = -2;
                            a[x - 1][y] = -2;
                            a[x + 1][y] = -2;
                            roundPlayer = 1;
                            hGard = false;
                            ziduri2[zid2]=0;
                            zid2--;
                        }

                            verify(3);
                            if (st) {
                                a[x][y] = 0;
                                a[x - 1][y] = 0;
                                a[x + 1][y] = 0;
                                roundPlayer = 2;
                                hGard = true;
                                zid2++;
                                ziduri2[zid2]=1;
                            }
                            verify(4);
                            if (st) {
                                a[x][y] = 0;
                                a[x - 1][y] = 0;
                                a[x + 1][y] = 0;
                                roundPlayer = 2;
                                hGard = true;
                                zid2++;
                                ziduri2[zid2]=1;
                            }
                        }
                    }
                }
        } 
}

function verify(pos) {
    for (i = 0; i < 17; i++) {
        result[i] = [];
        for (j = 0; j < 17; j++) {
            let valueObj = {
                check: false,
                left: false,
                down: false,
                right: false,
                up: false
            }
            result[i][j] = valueObj;
            if (a[i][j] == pos) {
                cui = i;
                cuj = j;
            }
        }
    }
    st = false;
    var checked = 0;
    var positions = 1;
    result[cui][cuj].check = true;
    var finish;
    if (pos == 3) finish = 16;
    if (pos == 4) finish = 0;
    while (!st && cui != finish) {

        if (cui + 1 <= 16) {
            if (a[cui + 1][cuj] == -1 || a[cui + 1][cuj] == -2 || a[cui + 1][cuj] == undefined) result[cui][cuj].down = true;
        } else result[cui][cuj].down = true;

        if (cuj + 1 <= 16) {
            if (a[cui][cuj + 1] == -1 || a[cui][cuj + 1] == -2 || a[cui][cuj + 1] == undefined) result[cui][cuj].right = true;
        } else result[cui][cuj].right = true;

        if (cuj - 1 >= 0) {
            if (a[cui][cuj - 1] == -1 || a[cui][cuj - 1] == -2 || a[cui][cuj - 1] == undefined) result[cui][cuj].left = true;
        } else result[cui][cuj].left = true;

        if (cui - 1 >= 0) {
            if (a[cui - 1][cuj] == -1 || a[cui - 1][cuj] == -2 || a[cui - 1][cuj] == undefined) result[cui][cuj].up = true;
        } else result[cui][cuj].up = true;

        if (cui + 1 < 17 && a[cui + 1][cuj] > -1 && (result[cui][cuj].down == false) &&
            (!result[cui + 2][cuj].down || !result[cui + 2][cuj].right || !result[cui + 2][cuj].left || !result[cui + 2][cuj].up)) {
            cui = cui + 2;
            result[cui][cuj].check = true;
            result[cui - 2][cuj].down = true;
        } else {
            result[cui][cuj].down = true;
            if (a[cui][cuj + 1] > -1 && (result[cui][cuj].right == false) &&
                (!result[cui][cuj + 2].down || !result[cui][cuj + 2].right || !result[cui][cuj + 2].left || !result[cui][cuj + 2].up)) {
                cuj = cuj + 2;
                result[cui][cuj].check = true;
                result[cui][cuj - 2].right = true;
            } else {
                result[cui][cuj].right = true;
                if ((a[cui][max(0, cuj - 1)] > -1) && (result[cui][cuj].left == false) &&
                    (!result[cui][cuj - 2].down || !result[cui][cuj - 2].right || !result[cui][cuj - 2].left || !result[cui][cuj - 2].up)) {
                    cuj = cuj - 2;
                    result[cui][cuj].check = true;
                    result[cui][cuj + 2].left = true;
                } else {
                    result[cui][cuj].left = true;
                    if (cui - 2 >= 0)
                        if (a[max(0, cui - 1)][cuj] > -1 && (result[cui][cuj].up == false) &&
                            (!result[cui - 2][cuj].down || !result[cui - 2][cuj].right || !result[cui - 2][cuj].left || !result[cui - 2][cuj].up)) {
                            cui = cui - 2;
                            result[cui][cuj].check = true;
                            result[cui + 2][cuj].up = true;
                        } else {
                            result[cui][cuj].up = true;
                        }
                }
            }
        }

        positions = 0;
        checked = 0;
        for (i = 0; i < 17; i++)
            for (j = 0; j < 17; j++) {
                if (result[i][j].check == true) positions++;
                if (result[i][j].left && result[i][j].up && result[i][j].down && result[i][j].right) checked++;
            }

        if (result[cui][cuj].left && result[cui][cuj].up && result[cui][cuj].down && result[cui][cuj].right) {
            st = true;
        }
        if (st) {
            for (i = 0; i < 17; i++)
                for (j = 0; j < 17; j++) {
                    if (result[i][j].check && (!result[i][j].left || !result[i][j].up || !result[i][j].right || !result[i][j].down)) {
                        cui = i;
                        cuj = j;
                        st = false;
                    }
                }

        }
    }
}




function White() {
    if (roundPlayer == 1) {
        hGard = false;
        vGard = false;
        moveWhite = true;
        moveBlack = false;
    }
}

function Black() {
    if (roundPlayer == 2) {
        hGard = false;
        vGard = false;
        moveBlack = true;
        moveWhite = false;
    }
}

function vGard1() {
    if (roundPlayer == 1) {
        hGard = false;
        vGard = true;
        moveWhite = false;
        moveBlack = false;
    }
}

function vGard2() {
    if (roundPlayer == 2) {
        hGard = false;
        vGard = true;
        moveWhite = false;
        moveBlack = false;
    }
}

function hGard1() {
    if (roundPlayer == 1) {
        vGard = false;
        hGard = true;
        moveWhite = false;
        moveBlack = false;
    }
}

function hGard2() {
    if (roundPlayer == 2) {
        vGard = false;
        hGard = true;
        moveWhite = false;
        moveBlack = false;
    }
}