class Table {

    static matrice() {
        erase();
        rect(0, 0, 200, 600);
        noErase();

        // tabla propriu zisa
        fill("pink");
        square(200, 0, 600);

        //blocurile pe tabla de joc
        fill('purple');
        for (let i = 0; i < 17; i++) {
            for (let j = 0; j < 17; j++) {
                if(i%2==0 && j%2==0)
                if (a[i][j] != 0)
                    square(288 + i * 25, 80 + j * 25, 40);
            }
        }
    }

    static pozitieTabla() {

        //dungile gri
        fill("blue");
        rect(270, 65, 460, 10);
        rect(270, 525, 460, 10);

        //marginile dintre dungi
        fill('turquoise');
        for (let i = 0; i < 9; i++) {
            rect(280 + i * 40 + i * 10, 5, 40, 60);
            rect(280 + i * 40 + i * 10, 535, 40, 60);


        }

    }

    static ziduri() {

        //zidurile de sus
        fill('white');
        for (let i = 0; i < 10; i++)
            if (ziduri1[i] != 0) {
                rect(270 + i * 40 + i * 10, 0, 10, 65);
            }

        //zidurile de jos
        fill('black');
        for (let i = 0; i < 10; i++)
            if (ziduri2[i] != 0) {
                rect(270 + i * 40 + i * 10, 535, 10, 65);
            }
          
     }


}